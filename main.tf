#Define cloud provider
provider "azurerm" {
   version = "~>2.0"
   features {}
}

# Configure state to be saved in Storage Account
terraform {
  backend "azurerm" {
    resource_group_name  = "2021-am-int-01"
    storage_account_name = "terrafortza1"
    container_name       = "container-fortza"
    key                  = "terraform.tfstate"
  }
}

