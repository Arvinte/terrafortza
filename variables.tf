variable "resource_sufix" {
    type        = "string"
    default     = "fortza"
    description = "Prefix pentru numirea resurselor."
}

variable "location" {
    type        = "string"
    default     = "westeurope"
    description = "Regiunea pentru deploy."
}

variable "username" {
    type        = "string"
    default     = "Fortzini"
    description = "Username pentru VM-uri."
}

variable "resource_group" {
    type        = "string"
    default     = "2021-am-int-01"
    description = "resource group name"
}