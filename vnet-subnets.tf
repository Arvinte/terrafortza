# Create virtual network with public and private subnets.
resource "azurerm_virtual_network" "vnet" {
    name                = "vnet-${var.resource_sufix}"
    address_space       = ["10.0.0.0/16"]
    location            = "${var.location}"
    resource_group_name = "azurerm_resource_group.${var.resource_group}"

}
 
# Create public subnet for hosting bastion and list service endpoints to associate with the subnet
resource "azurerm_subnet" "public_subnet" {
  name                      = "bast-${var.resource_sufix}"
  resource_group_name       = "azurerm_resource_group.${var.resource_group}"
  virtual_network_name      = "${azurerm_virtual_network.vnet.name}"
  address_prefix            = "10.0.0.0/24"
  
}

# Create private subnet for hosting bastion and list service endpoints to associate with the subnet
resource "azurerm_subnet" "private_app_subnet" {
  name                      = "private-app-${var.resource_sufix}"
  resource_group_name       = "azurerm_resource_group.${var.resource_group}"
  virtual_network_name      = "${azurerm_virtual_network.vnet.name}"
  address_prefix            = "10.0.1.0/24"
  
}

# Create private subnet for hosting bastion and list service endpoints to associate with the subnet
resource "azurerm_subnet" "private_db_subnet" {
  name                      = "private-db-${var.resource_sufix}"
  resource_group_name       = "azurerm_resource_group.${var.resource_group}"
  virtual_network_name      = "${azurerm_virtual_network.vnet.name}"
  address_prefix            = "10.0.2.0/24"
  
}

